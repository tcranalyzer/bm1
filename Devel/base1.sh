if [ "$study_name" = "" ]; then
    echo "No study_name set. Exiting."
elif [ "$count_cutoff" = "" ];then
    echo "No count_cutoff set. Exiting."
else
    Rscript Devel/BasisModule1.R $study_name $count_cutoff
fi
