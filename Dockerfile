FROM r-base:4.3.2

RUN apt update -y
RUN apt upgrade -y
RUN apt install build-essential libcurl4-gnutls-dev libxml2-dev libssl-dev libfontconfig1-dev libharfbuzz-dev  libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev cmake -y

RUN R -e "install.packages('devtools')"
RUN R -e "devtools::install_github('immunomind/immunarch')"
RUN R -e "install.packages('qs')"

WORKDIR /app
COPY ../ /app/

ENTRYPOINT ["sh", "Devel/base1.sh"]